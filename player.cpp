// Student 1: Connor Crowley
// Student 2: Victor Venturi

#include "player.h"
#include <vector>
#include <climits>

using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    Board *brd = new Board();

    this->ourBoard = brd;
    this->color = side;

    for (int i = 1; i < 6; ++i)
    {
        for (int j = 1; j < 6; ++j)
        {
            this->weights[i][j] = 0;
        }
    }

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (i == 0 || j == 0 || i == 7 || j == 7)
            {
                this->weights[i][j] = 10;
            }
        }
    }

    this->weights[0][0] = 500;
    this->weights[0][7] = 500;
    this->weights[7][0] = 500;
    this->weights[7][7] = 500;

    this->weights[0][1] = -150;
    this->weights[1][0] = -150;
    this->weights[1][1] = -250;

    this->weights[6][7] = -150;
    this->weights[7][6] = -150;
    this->weights[6][6] = -250;

    this->weights[6][0] = -150;
    this->weights[6][1] = -250;
    this->weights[7][1] = -150;

    this->weights[0][6] = -150;
    this->weights[1][6] = -250;
    this->weights[1][7] = -150;

    this->weights[0][2] = 30;
    this->weights[0][5] = 30;
    this->weights[2][0] = 30;
    this->weights[2][7] = 30;
    this->weights[5][0] = 30;
    this->weights[5][7] = 30;
    this->weights[7][2] = 30;
    this->weights[7][5] = 30;

    /*this->weights[0][3] = 1;
    this->weights[0][4] = 1;
    this->weights[3][0] = 1;
    this->weights[4][0] = 1;
    this->weights[3][7] = 1;
    this->weights[4][7] = 1;
    this->weights[7][3] = 1;
    this->weights[7][4] = 1;*/

    this->weights[2][3] = 2;
    this->weights[2][4] = 2;
    this->weights[3][2] = 2;
    this->weights[3][5] = 2;
    this->weights[4][2] = 2;
    this->weights[4][5] = 2;
    this->weights[5][3] = 2;
    this->weights[5][4] = 2;

    this->weights[2][2] = 1;
    this->weights[2][5] = 1;
    this->weights[5][2] = 1;
    this->weights[5][5] = 1;

    this->weights[3][3] = 16;
    this->weights[3][4] = 16;
    this->weights[4][3] = 16;
    this->weights[4][4] = 16;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft)
{
    if (this->testingMinimax == true)
    {
        return doMinimaxMove(opponentsMove, msLeft);
    }
    else
    {
        return doHeuristicMove(opponentsMove, msLeft);
    }
}

Move *Player::doMinimaxMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Side opp = this->color == WHITE ? BLACK : WHITE;

    //int weighted_score = this->weightedScore(this->ourBoard, this->color);
    if (opponentsMove != NULL)
    {
        if (this->color == BLACK)
        {
            this->ourBoard->doMove(opponentsMove, WHITE);
        }
        else
        {
            this->ourBoard->doMove(opponentsMove, BLACK);
        }
    }

    vector<Move *> *mvVect = new vector<Move *>;
    mvVect = this->possibleMoves(this->ourBoard, this->color);

    if (mvVect->size() == 0)
    {
        return NULL;
    }

    Board *brdCopy = this->ourBoard->copy();

    vector<Board *> *brdVect = new vector<Board *>;
    brdVect = this->processMoves(mvVect, brdCopy, this->color);

    int length = brdVect->size();

    int amount = 0;
    int index = 0;
    int currMax = INT_MIN;
    cerr << "size mvVect " << mvVect->size() << endl;
    for (int i = 0; i < length; ++i)
    {
        vector<Board *> *brdVectChildren = new vector<Board *>;
        brdVectChildren = this->getChildrenBoards((*brdVect)[i], opp);
        amount = this->minScore(brdVectChildren);
        //cerr << "x " << (*mvVect)[i]->getX() << " y " << (*mvVect)[i]->getY() << endl;
        if (amount > currMax)
        {
            index = i;
            currMax = amount;
        }
    }

    this->ourBoard->doMove((*mvVect)[index], this->color);
    return (*mvVect)[index];
}

Move *Player::doHeuristicMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    //int weighted_score = this->weightedScore(this->ourBoard, this->color);
    int temporary_score = -100000000;
    Move *ourMove = new Move(0, 0);
    if (opponentsMove != NULL)
    {
        if (this->color == BLACK)
        {
            this->ourBoard->doMove(opponentsMove, WHITE);
        }
        else
        {
            this->ourBoard->doMove(opponentsMove, BLACK);
        }
    }

    if (this->ourBoard->hasMoves(this->color))
    {
        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 8; ++j)
            {
                Move *newMove = new Move(i, j);
                if (this->ourBoard->checkMove(newMove, this->color))
                {
                    Board *alternativeBoard = new Board();
                    *alternativeBoard = *(this->ourBoard);
                    alternativeBoard->doMove(newMove, this->color);
                    if (temporary_score < this->weightedScore(alternativeBoard, this->color))
                    {
                        temporary_score = this->weightedScore(alternativeBoard, this->color);                  
                        *ourMove = *(newMove);
                    }
                }
            }
        }
        this->ourBoard->doMove(ourMove, this->color);
        return ourMove;
    }
    
    return NULL;
}

vector<Board *> *Player::getChildrenBoards(Board *brd, Side side)
{
    vector<Move *> *mvVect = new vector<Move *>;
    mvVect = this->possibleMoves(brd, side);

    Board *brdCopy = brd->copy();
    vector<Board *> *brdVect = new vector<Board *>;
    brdVect = this->processMoves(mvVect, brdCopy, side);

    return brdVect;
}

/*
 * This function takes a pointer to a board as an input parameter
 * and returns a vector of pointers to valid moves on the board
 * passed in as input.  Further, this functionn takes a Side as
 * input so that we know whose turn it is.
 */
vector<Move *> *Player::possibleMoves(Board *brd, Side side)
{
    vector<Move *> *mvVect = new vector<Move *>;
    if (brd->hasMoves(side))
    {
        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 8; ++j)
            {
                Move *mv = new Move(i, j);
                if (brd->checkMove(mv, side))
                {
                    mvVect->push_back(mv);
                }
            }
        }
    }
    return mvVect;
}

int Player::minScore(vector<Board *> *brdVect)
{
    int length = brdVect->size();

    int temp, minScore;

    minScore = INT_MAX;

    for (int i = 0; i < length; ++i)
    {
        temp = this->score((*brdVect)[i]);
        if (temp < minScore)
        {
            minScore = temp;
        }
    }
    return minScore;
}

/*
 * This function takes a pointer to a board as an input parameter
 * and returns an int that represents the score of the board
 * pointed to by the input pointer.
 */
int Player::score(Board *brd)
{
    if (this->color == BLACK)
    {
        return brd->countBlack() - brd->countWhite();
    }
    else //this->color == WHITE
    {
        return brd->countWhite() - brd->countBlack();
    }
}

vector<Board *> *Player::processMoves(vector<Move *> *mvVect, Board *brd, Side side)
{
    int length = mvVect->size();
    vector<Board *> *brdVect = new vector<Board *>;

    for (int i = 0; i < length; ++i)
    {
        Board *tempBrd = brd->copy();
        tempBrd->doMove((*mvVect)[i], side);
        brdVect->push_back(tempBrd);
    }

    return brdVect;
}

int Player::weightedScore (Board *board, Side side)
{
    int score = 0;
    if (side == BLACK)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                score += (this->weights[i][j])*(board->whichColor(i, j));
            }
        }
    }
    else
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                score -= (this->weights[i][j])*(board->whichColor(i, j));
            }
        }
    }
    return score;
}
