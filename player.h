#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {
private:
	Side color;
    int weights[8][8];

public:
	Board *ourBoard;
    Player(Side side);
    ~Player();

    Move *doMove(Move *opponentsMove, int msLeft);

    Move *doMinimaxMove(Move *opponentsMove, int msLeft);

    Move *doHeuristicMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    vector<Board *> *getChildrenBoards(Board *brd, Side side);
    vector<Move *> *possibleMoves(Board *brd, Side side);
    int minScore(vector<Board *> *brdVect);
    int score(Board *brd);
    vector<Board *> *processMoves(vector<Move *> *mvVect, Board *brd, Side side);
    int weightedScore(Board *board, Side side);
};

#endif
