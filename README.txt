// Author1: Connor Crowley
// Author2: Victor Venturi

Connor Crowley
-Made initial commit before Sunday night
-Created the README.txt file
-Wrote the minimax algorithm
-Discussed heuristic implementation at a higher level

Victor Venturi
-Made initial commit before Sunday night
-Implemented Player() and *doMove()
-Implemented simple heuristic function to beat SimplePlayer
-Discussed minimax implementation at a higher level

Discussion of our strategy:
Our strategy is: for the first moves, we will use our minimax tree coupled with
our heuristics function to compute the best move. However, after all the corners
and all the edges are already taken by pieces OR when we realize that the game
will end in at most five moves, we will stop using the heuristics to calculate
the value of a board and start just using the difference between the number of
pieces of each color. We believe our strategy is good because the heuristics
were implemented having in mind that the corners and edges were more valuable
than all other squares. However, once they are all taken, it does not make sense
to give them more weight than other squares. Also, when the game is too close to
the end, what really matters is the number of pieces we have, and not their
weight on the board.
